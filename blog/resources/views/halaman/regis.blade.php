<!DOCTYPE html>
<html>
    <head>
        <title>
            JCC BATC 2 BAG 12 LARAVEL
        </title>
    </head>
    <body>
        <h1>Buat Account Baru</h1>
        <h4>Sign Up Form</h4>
        <form action="/submit" method="post">
            @csrf
            <label>First Name :</label><br>
            <input type="text" name="firstname" required><br> <br>
            <label>Last Name :</label><br>
            <input type="text" name="lastname" required><br> <br>
            <label>Gender</label> <br>
            <input type="radio" name="Gender" value="1">Male <br>
            <input type="radio" name="Gender" value="2">Female <br>
            <input type="radio" name="Gender" value="2">Other <br> <br>
            <label>Nationality</label>
            <select name="nationality">
                <option value="1">Indonesia</option>
                <option value="2">Rusia</option>
                <option value="3">Ukraina</option>
            </select><br> <br>
            <label >Language Spoken</label> <br>
            <input type="checkbox" name="Language"> Bahasa Indonesia <br>
            <input type="checkbox" name="Language"> English <br>
            <input type="checkbox" name="Language"> Arab <br>
            <input type="checkbox" name="Language"> Other<br> <br>
            <label>Bio</label> <br>
            <textarea name="bio" cols="30" rows="10" required></textarea>
            <br> <br>
            <input type="submit" value="Sign Up">
        </form>
    </body>
</html>
